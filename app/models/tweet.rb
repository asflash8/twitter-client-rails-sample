class Tweet < ActiveRecord::Base
  Twitter.configure do |config|
    config.consumer_key       = 'YOUR CONSUMER KEY'
    config.consumer_secret    = 'YOUR CONSUMER SECRET'
    config.oauth_token        = 'YOUR OAUTH TOKEN'
    config.oauth_token_secret = 'YOUR OAUTH TOKEN SECRET'
  end

  def self.search(keyword, options = {})
    Twitter.search(keyword || '#rubyjp').results
  end

  def self.update(text)
    Twitter.update(text)
  end

  def self.status(status_id)
    Twitter.status(status_id)
  end
end
