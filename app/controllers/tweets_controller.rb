class TweetsController < ApplicationController
  before_action :set_tweet, only: [:show, :edit, :update, :destroy]

  # GET /tweets
  def index
    @search_keyword = params[:tweet][:q] if params[:tweet]
    @search_keyword ||= '#rubyjp'
    @tweets = Tweet.search(@search_keyword)
  end

  # GET /tweets/1
  def show
  end

  # GET /tweets/new
  def new
    @tweet = Tweet.new
  end

  # GET /tweets/1/edit
  def edit
  end

  # POST /tweets
  def create
    @tweet = Tweet.new(tweet_params)

    if Tweet.update("#{@tweet.content} by #{@tweet.from_user_name} at #{Time.now.strftime('%Y/%m/%d %H:%M:%S')} via NtsTweet")
      redirect_to @tweet, notice: 'Tweet was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /tweets/1
  def update
    if @tweet.update(tweet_params)
      redirect_to @tweet, notice: 'Tweet was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /tweets/1
  def destroy
    @tweet.destroy
    redirect_to tweets_url, notice: 'Tweet was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tweet
      @tweet = Tweet.status(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def tweet_params
      params.require(:tweet).permit(:id, :content, :from_user_id, :from_user_name)
    end
end
