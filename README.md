Railsを使ってTwitter検索とTweetしてみよう
------

1 このリポジトリを取得する

    git clone https://asflash8@bitbucket.org/asflash8/twitter-client-rails-sample.git

2 gemのインストール

    cd twitter-client-rails-sample && bundle install --path vendor/bundle

3 gitで作業用ブランチを作成とタグをチェックアウト

    git pull --all
    git checkout -b work v0.1

4 Gemfileに以下を追記

    group :assets do
      gem 'coffee-rails', '~> 4.0.0'
      gem 'uglifier', '>= 1.3.0'
    end

5 ライブラリのインストール

    bundle install --path vendor/bundle

6 scaffoldでひな型を作成

    bundle exec rails g scaffold tweet content:text screen_name:string status_id:integer profile_image_url:string

7 データベースのマイグレート

    bundle exec rake db:migrate

8 Railsの起動

    bundle exec rails s
    #http://192.168.8.31:3000にアクセス

9 tweet検索の作成

  - モデルの作成
    - searchメソッドの実装
  - コントローラーの作成
    - indexアクションでsearchメソッドを呼び出す
  - ビューの作成
    - コントローラーで取得したtweetを一覧で表示

10 tweetしてみる

  - モデルの作成
    - updateメソッドの実装
  - コントローラーの作成
    - createアクションでupdateメソッドを呼び出す
  - ビューの作成
    - formの作成

11 AWSへデプロイしてみる

    eb init
    eb start
    git aws.push
