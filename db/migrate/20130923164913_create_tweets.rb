class CreateTweets < ActiveRecord::Migration
  def change
    create_table :tweets do |t|
      t.integer :tweet_id
      t.text :content
      t.integer :from_user_id
      t.string :from_user_name

      t.timestamps
    end
  end
end
